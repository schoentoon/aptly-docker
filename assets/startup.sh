#!/bin/sh

mkdir -p /opt/aptly/public

# If we just don't have the signing key we generate one
if [[ ! -f /opt/aptly/aptly.secret.key ]]; then
  echo "Generating new gpg keys"
  cat << EOF > /tmp/gpg_batch
%echo Generating a GPG key, might take a while
Key-Type: RSA
Key-Length: 4096
Name-Real: ${FULL_NAME}
Name-Comment: Aptly Repo Signing
Name-Email: ${EMAIL_ADDRESS}
Expire-Date: 0
%no-protection
%commit
%echo done
EOF
  gpg --verbose --batch --gen-key /tmp/gpg_batch
  rm /tmp/gpg_batch
  gpg --verbose --batch --export-secret-keys --armor > /opt/aptly/aptly.secret.key
else
  # if we do have it, then we import it instead
  gpg --verbose --batch --import /opt/aptly/aptly.secret.key
fi

if [[ ! -f /opt/aptly/public/aptly_repo_signing.key ]]; then
  gpg --verbose --batch --export --armor > /opt/aptly/public/aptly_repo_signing.key
fi

exec /usr/bin/aptly api serve -no-lock