package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"strconv"
	"time"
)

type Session struct {
	Host   string
	Dir    string
	Client *http.Client
}

type AddPackageResponse struct {
	FailedFiles []string `json:"FailedFiles"`
	Report      Report   `json:"Report"`
}

type Report struct {
	Warnings []string `json:"Warnings"`
	Added    []string `json:"Added"`
	Deleted  []string `json:"Deleted"`
}

func (s *Session) UploadFile(fname string) ([]string, error) {
	f, err := os.Open(fname)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	var buf bytes.Buffer
	w := multipart.NewWriter(&buf)
	fw, err := w.CreateFormFile("file", fname)
	if err != nil {
		return nil, err
	}
	if _, err = io.Copy(fw, f); err != nil {
		return nil, err
	}
	err = w.Close()
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", fmt.Sprintf("%s/api/files/%s", s.Host, s.Dir), &buf)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", w.FormDataContentType())

	res, err := s.Client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	var out []string
	err = json.NewDecoder(res.Body).Decode(&out)
	if err != nil {
		return nil, err
	}
	return out, nil
}

type AddRepoRequest struct {
	Name string `json:"Name"`
}

func (s *Session) CreateRepo(name string) error {
	in := AddRepoRequest{
		Name: name,
	}
	var buf bytes.Buffer
	err := json.NewEncoder(&buf).Encode(in)
	if err != nil {
		return err
	}

	req, err := http.NewRequest("POST", fmt.Sprintf("%s/api/repos", s.Host), &buf)
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")

	res, err := s.Client.Do(req)
	if err != nil {
		return err
	}
	return res.Body.Close()
}

func (s *Session) AddPackages(name string) (*AddPackageResponse, error) {
	req, err := http.NewRequest("POST", fmt.Sprintf("%s/api/repos/%s/file/%s", s.Host, name, s.Dir), nil)
	if err != nil {
		return nil, err
	}

	res, err := s.Client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	var out AddPackageResponse
	err = json.NewDecoder(res.Body).Decode(&out)
	if err != nil {
		return nil, err
	}
	return &out, nil
}

type PublishRequest struct {
	Distribution  string          `json:"Distribution"`
	SourceKind    string          `json:"SourceKind"`
	Sources       []PublishSource `json:"Sources"`
	Signing       SigningOptions  `json:"Signing`
	Architectures []string        `json:"Architectures,omitempty"`
}

type PublishSource struct {
	Name string `json:"Name"`
}

type SigningOptions struct {
	Passphrase string `json:"Passphrase"`
}

type PublishResult struct {
	Error         string   `json:"Error"`
	AcquireByHash bool     `json:"AcquireByHash"`
	Architectures []string `json:"Architectures"`
	Distribution  string   `json:"Distribution"`
	Label         string   `json:"Label"`
	Origin        string   `json:"Origin"`
	Prefix        string   `json:"Prefix"`
}

func (s *Session) PublishRepo(distribution, name, passphrase, arch string) (*PublishResult, error) {
	in := PublishRequest{
		Distribution: distribution,
		SourceKind:   "local",
		Sources: []PublishSource{
			PublishSource{
				Name: name,
			},
		},
		Signing: SigningOptions{
			Passphrase: passphrase,
		},
	}
	if arch != "" {
		in.Architectures = []string{arch}
	}

	var buf bytes.Buffer
	err := json.NewEncoder(&buf).Encode(in)
	if err != nil {
		return nil, err
	}
	req, err := http.NewRequest("POST", fmt.Sprintf("%s/api/publish", s.Host), &buf)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")

	res, err := s.Client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	var out PublishResult

	if res.StatusCode == 400 {
		var buf bytes.Buffer
		err := json.NewEncoder(&buf).Encode(in)
		if err != nil {
			return nil, err
		}
		req, err = http.NewRequest("PUT", fmt.Sprintf("%s/api/publish/:./%s", s.Host, distribution), &buf)
		if err != nil {
			return nil, err
		}
		req.Header.Set("Content-Type", "application/json")

		res, err = s.Client.Do(req)
		if err != nil {
			return nil, err
		}
		defer res.Body.Close()
	}

	err = json.NewDecoder(res.Body).Decode(&out)
	if err != nil {
		return nil, err
	}

	return &out, nil
}

func (s *Session) DropDirectory() error {
	req, err := http.NewRequest("DELETE", fmt.Sprintf("%s/api/files/%s", s.Host, s.Dir), nil)
	if err != nil {
		return err
	}
	res, err := s.Client.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()
	return nil
}

func main() {
	createRepo := flag.Bool("create-repo", false, "Should we try to create the repo first?")
	host := flag.String("host", "", "Host of the aptly api, please include http:// or https://")
	name := flag.String("name", "", "Name of the repository to add our packages to")
	distribution := flag.String("distribution", "", "Distribution to publish")
	gpgPassphrase := flag.String("passphrase", "", "Passphrase for gpg signing")
	arch := flag.String("arch", "", "Architecture to publish as")
	flag.Parse()

	if *host == "" || *name == "" || *distribution == "" {
		flag.PrintDefaults()
		os.Exit(1)
	}

	session := &Session{
		Host:   *host,
		Dir:    strconv.FormatInt(time.Now().UnixNano(), 10),
		Client: http.DefaultClient,
	}

	defer session.DropDirectory()

	files := []string{}
	for _, fname := range flag.Args() {
		f, err := session.UploadFile(fname)
		if err != nil {
			log.Println(err)
			continue
		}
		files = append(files, f...)
	}

	if *createRepo {
		_ = session.CreateRepo(*name)
	}

	report, err := session.AddPackages(*name)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Added %#v", report)

	result, err := session.PublishRepo(*distribution, *name, *gpgPassphrase, *arch)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Published %#v", result)
	if result.Error != "" {
		os.Exit(1)
	}
}
