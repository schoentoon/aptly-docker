FROM golang:alpine AS builder

RUN apk add git make

RUN git clone https://github.com/aptly-dev/aptly

ARG VERSION=master

RUN cd aptly && git checkout $VERSION

RUN cd aptly && make install

FROM alpine:latest

COPY --from=builder /go/bin/aptly /usr/bin/aptly

RUN apk add gnupg curl

RUN adduser -D aptly

USER aptly

COPY assets/aptly.conf /home/aptly/.aptly.conf
COPY assets/startup.sh /home/aptly/startup.sh

EXPOSE 8080/tcp

VOLUME [ "/opt/aptly" ]

ENTRYPOINT [ "/home/aptly/startup.sh" ]